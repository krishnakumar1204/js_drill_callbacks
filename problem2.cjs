/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/


const fs = require("fs");

const fileNamesPath = "./filenames.txt";

function textManupulation(inputFilePath) {
    
    if ((typeof inputFilePath === 'string') && inputFilePath !== '') {

        // Reading the "lipsum.txt" file.
        fs.readFile(inputFilePath, 'utf-8', (error, data) => {

            if (error) {
                console.log(error);
            }
            else {

                // After converting the content of "lipsum.txt" into upperCase and writing it in new file "upperCase.txt"
                data = data.toUpperCase();
                let upperCaseFileName = "upperCase.txt";

                fs.writeFile(`./${upperCaseFileName}`, data, (error) => {
                    if (error) {
                        console.log(error);
                    }
                    else {
                        console.log("upperCase.txt created");

                        // storing the file-name of "upperCase.txt" in "filenames.txt" file
                        const fileNamesPath = "./filenames.txt";

                        fs.writeFile(fileNamesPath, upperCaseFileName, (error) => {
                            if (error) {
                                console.log(error);
                            }
                            else {

                                console.log("upperCaseFileName stored in filenames.txt");

                                // reading the "upperCase.txt"
                                fs.readFile(`./${upperCaseFileName}`, 'utf-8', (error, upperCaseFileData) => {
                                    if (error) {
                                        console.log(error);
                                    }
                                    else {

                                        let lowerCaseFileData = upperCaseFileData.toLowerCase();
                                        let sentences = lowerCaseFileData.split('.');
                                        sentences = sentences.map((sentence) => sentence.trim()).filter((sentence) => sentence.length > 0);
                                        sentences = sentences.join('\n');

                                        let sentencesFileName = "sentences.txt";

                                        // writing the content to "sentences.txt" file after coverting them to sentences
                                        fs.writeFile(`./${sentencesFileName}`, sentences, (error) => {

                                            if (error) {
                                                console.log(error);
                                            }
                                            else {
                                                console.log("sentences.txt created");

                                                // storing the file-name of "sentences.txt" in "filenames.txt" file
                                                fs.writeFile(fileNamesPath, `\n${sentencesFileName}`, { flag: 'a' }, (error) => {

                                                    if (error) {
                                                        console.log(error);
                                                    }
                                                    else {
                                                        console.log("sentencesFileName stored in filenames.txt");

                                                        // Reading the content of "sentences.txt"
                                                        fs.readFile(`./${sentencesFileName}`, 'utf-8', (error, sentences) => {

                                                            if (error) {
                                                                console.log(error);
                                                            }
                                                            else {
                                                                sentences = sentences.split('\n');

                                                                // sorting the content of the "sentences.txt"
                                                                let sortedData = sentences.sort();
                                                                sortedData = sortedData.join('.\n');

                                                                let newFileName = "newFile.txt";

                                                                // writing the content of "sentences.txt" into "newFile.txt" after soting the sentences
                                                                fs.writeFile(`./${newFileName}`, sortedData, (error) => {

                                                                    if (error) {
                                                                        console.log(error);
                                                                    }
                                                                    else {

                                                                        console.log("newFile.txt created!");

                                                                        // storing the file-name of "newFile.txt" in "filenames.txt"
                                                                        fs.writeFile(fileNamesPath, `\n${newFileName}`, { flag: 'a' }, (error) => {
                                                                            if (error) {
                                                                                console.log(error);
                                                                            }
                                                                            else {

                                                                                console.log("newFileName stored in filenames.txt");

                                                                                // reading the content of the "filenames.txt" file
                                                                                fs.readFile(fileNamesPath, 'utf-8', (error, filenames) => {
                                                                                    if (error) {
                                                                                        console.log(error);
                                                                                    }
                                                                                    else {
                                                                                        filenames = filenames.split('\n');

                                                                                        // deleting all files whose names are stored in "filenames.txt" file
                                                                                        filenames.forEach(filename => {
                                                                                            fs.unlink(`./${filename}`, (error) => {
                                                                                                if (error) {
                                                                                                    console.log(error);
                                                                                                }
                                                                                                else {
                                                                                                    console.log(`${filename} deleted!`);
                                                                                                }
                                                                                            })
                                                                                        });
                                                                                    }
                                                                                })
                                                                            }
                                                                        })
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
}

module.exports = textManupulation;