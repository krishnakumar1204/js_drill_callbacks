/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/


const fs = require("fs");


function createAndDeleteFile(dirPath) {

    if ((typeof dirPath === 'string') && (dirPath !== '')) {

        fs.mkdir(dirPath, { recursive: true }, (error) => {
            if (error) {
                console.log(error);
            }
            else {
                console.log("Directory Created");

                // using fs module's async functions
                let count = 0;
                let numberOfFiles = Math.floor(Math.random() * 10 + 1);

                function createAndDelete(count, numberOfFiles) {
                    if ((typeof numberOfFiles==='number') && (numberOfFiles != NaN) && (count < numberOfFiles)) {

                        // const ranNum = Math.floor(((Math.random() * numberOfFiles) + 1));
                        const filePath = `${dirPath}/File${count}.json`;

                        fs.writeFile(filePath, "", (err) => {
                            if (err) {
                                console.log(err);
                            }
                            else {
                                console.log(`File${count}.json Created!`);
                                fs.unlink(filePath, (er) => {
                                    if (err) {
                                        console.log(er);
                                    }
                                    else {
                                        console.log(`File${count}.json deleted!`);

                                        createAndDelete(count + 1, numberOfFiles);
                                    }
                                });
                            }
                        });
                    }
                }
                createAndDelete(count, numberOfFiles);


                // using fs module's synchronous functions

                // for(let i=0; i<10; i++){
                //     const ranNum = Math.floor(Math.random()*9 + 1);
                //     const filePath = `${dirPath}/randomFile${ranNum}.json`;

                //     fs.writeFileSync(filePath);
                //     console. log(`randomFile${ranNum}.json Created!`);

                //     fs.unlinkSync(filePath);
                //     console.log(`randomFile${ranNum}.json deleted!`);

                // }

            }
        });
    }
    else{
        console.log("Directory Path is Invalid!");
    }
}


module.exports = createAndDeleteFile;